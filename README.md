# Signzy Internship Assignment

Image Quality Detector which classifies whether the image is blurry or not.

# Installation

You will have to install imutils before you run the scripts. This can be done using pip install imutils.
Also, as the size of Evaluation Set was 2GB, I decided to not upload it. **You will have to add DigitalBlurSet and NaturalBlurSet folders to the repo before running scripts1 and scripts2.**

# Usage Instructions

Since, The Evaluation Set contains 2 different folders, I decided to write 2 scripts.

1. Both Script can be run on any shell(I ran it on CMD) with the command. 

           `py Script1.py --DigitalBlurSet DigitalBlurSet`
       
           `py Script2.py --NaturalBlurSet NaturalBlurSet`


2. This will generate two excel files which contains file name and blur label as its columns, which has been uploaded as well for convinience of the examiner.

3. To evaluate the performance, score_evaluation can be run which will print the score using the 2 excel files created after running the scripts. Note that this file does not need any arguements like the previous 2 scripts to run.

# Logic

As given in a link in the reference part of the Assignment, I took help of Laplacian kernel.Laplacian kernel is often used for edge detection in Image processing.
The function variance_of_laplacian returns us with a single value which is the variance of the response obtained by convolving the grayscale version of the image against the laplacian kernel.
A threshold value can be used as a kind of decision boundary for our classifier. Variance below this value would indicate less variance has hence less edges ultimately meaning more blurry picture and vice-versa.

As you can see, Arguement Parsing has been used inside the scripts.The argparse module makes it easy to write user-friendly command-line interfaces which is why it is being used here. 

# References

https://www.pyimagesearch.com/2015/09/07/blur-detection-with-opencv/