import pandas as pd

#Importing excel sheets 
D1 = pd.read_excel('ans_DBS.xlsx')   # My results on DigitalBlurSet images
D1 = D1[D1.columns[1]]
N1 = pd.read_excel("ans_NBS.xlsx")   # My results on NaturalBlurSet images
N1 = N1[N1.columns[1]]

N2 = pd.read_excel("NaturalBlurSet.xlsx") # Actual results
N2 = N2[N2.columns[1]]
D2 = pd.read_excel("DigitalBlurSet.xlsx") # Actual results
D2 = D2[D2.columns[1]]

correct = 0
total = 0

# Comaparing DigitalBlurSet results
for i in range(len(D1)):
    if D1[i] == D2[i]:
        correct += 1
    total += 1

#Comapring NAturalBlurSet results
for i in range(len(N1)):
    if N1[i] == N1[i]:
        correct += 1
    total +=1
    
score = correct/total
print("Score:{0:.5f}".format(score))