# USAGE
# python detect_blur.py --DigitalBlurSet DigitalBlurSet

# import the necessary packages
from imutils import paths
import argparse
import cv2
import os
import numpy as np
import pandas as pd
from pandas import ExcelWriter


def variance_of_laplacian(image):
    # compute the Laplacian of the image and then return the focus
    # measure, which is simply the variance of the Laplacian
    return cv2.Laplacian(image, cv2.CV_64F).var()


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--DigitalBlurSet", required=True,
                help="path to input directory of images")
ap.add_argument("-t", "--threshold", type=float, default=100.0,
                help="focus measures that fall below this value will be considered 'blurry'")
args = vars(ap.parse_args())

d = {'File': [], 'Blur Label': []}
df = pd.DataFrame(data=d)

# ReArranging column positions
df = df[['File','Blur Label']]
result = []

# loop over the input images
for i, imagePath in enumerate(paths.list_images(args["DigitalBlurSet"])):
    # load the image, convert it to grayscale, and compute the
    # focus measure of the image using the Variance of Laplacian
    # method

    image = cv2.imread(imagePath)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    fm = variance_of_laplacian(gray)

    #If the focus measure is beyond a threshold value, label it as 1 else -1
    if fm < args["threshold"]:
        result = [imagePath[15:], 1]
    else:
        result = [imagePath[15:], -1]
    
    #Add the results to a dataframe
    df.loc[i] = result


#Convert the dataframe into an excel file which can be later used for comapring and finding the accuracy
writer = pd.ExcelWriter('ans_DBS.xlsx')
df.to_excel(writer,'Sheet1')
writer.save()



